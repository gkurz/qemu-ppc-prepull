#! /bin/sh

set -ex

git clone https://gitlab.com/dgibson/qemu.git --branch ppc-for-6.1

mkdir -p qemu/build/$VARIANT
cd qemu/build/$VARIANT

../../configure --cross-prefix=$CC_PREFIX

make -j8

ls -l qemu-img.exe
file qemu-img.exe
