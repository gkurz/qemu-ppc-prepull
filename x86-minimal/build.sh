#! /bin/sh

set -ex

git clone https://gitlab.com/dgibson/qemu.git --branch ppc-for-6.1

mkdir -p qemu/build/$VARIANT
cd qemu/build/$VARIANT

../../configure "$@"

make -j8
